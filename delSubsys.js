const getReq = require('./req/getReq')
const delReq = require('./req/delReq')

/*
Subsystem: /ws/policy/subsystems/
Team: /ws/policy/teams
Applications: /ws/policy/applications/
Instances: 
    Del: /ws/policy/instances/*+id*
    Get: /ws/policy/instances/?role=team

*/

let flag1 = false
let flag2 = false
let flag3 = false
let flag4 = false

const delSubsys = async() => {
    //*****INSTANCES*****\\
    await getReq('/ws/policy/instances/', (responseString) => {
        try{
            let pathToDel = '/ws/policy/instances/' + responseString.instances[0].id
            console.log(responseString.instances)
            delReq(pathToDel, (res) => {
                console.log(res)
            })
        } catch(error) {
            console.log('No instances to delete')
        }
    })

    //*****APPLICATIONS*****\\
await getReq('/ws/policy/applications/', (responseString) => {
    try{
        let pathToDel = '/ws/policy/applications/' + responseString.applications[0].id
        //console.log(responseString)
        delReq(pathToDel, (res) => {
            console.log(res)
        })
    } catch (error){
        console.log('No applications')
    }
    
})


//*****TEAMS******\\
await getReq('/ws/policy/teams', (resJson) => {
    try{
        //console.log(resJson)
        let teamIds = [resJson.teams[0].id, resJson.teams[0].environments[0].id]
        console.log(teamIds)
        let pathToDel = '/ws/policy/teams/' + resJson.teams[0].id
        delReq(pathToDel, (res) => {
            console.log(res)
        })
    } catch(error){
        console.log('No teams')
    }
    
})

//*****SUBSYSTEMS*****\\
await getReq('/ws/policy/subsystems/', (responseString) => {
    try{
        //console.log(responseString)
        let pathToDel = '/ws/policy/subsystems/' + responseString.subsystems[0].id
        delReq(pathToDel, (res) => {
            console.log(res)
        })
    } catch (error){
        console.log('No subsystems')
    }
    
})
}
delSubsys()

