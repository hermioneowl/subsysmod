const http = require('http')
//const fs = require('fs')

//const createOptions = require('../createOptions.js')

//let application = fs.readFileSync('./postJsons/application.json')
//let postBody = application
//application = JSON.parse(application)
//console.log(application)




const postReq = (postBody, options, callback) => {
    let req = http.request(options, function (res) {
        let responseString = "";
        res.on("data", function (data) {
            responseString += data;
            // save all the data from response
        });
        res.on("end", () => {
            //console.log(responseString)
            responseString = JSON.parse(responseString)
            callback(responseString)
            // print to console when response ends
        })
    })

    req.write(postBody)
    req.end()
}


module.exports = postReq