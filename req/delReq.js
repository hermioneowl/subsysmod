const http = require('http');
const getReq = require('./getReq')
// GET http://{{hostname}}:{{doeport}}/ws/policy/subsystems/
// content-type: application/json
// Authorization: Basic {{authDOEDBA}}
//"ZG9lZGJhOkMwREVTSE9QCg=="
//



//"/ws/policy/subsystems/a0f066e7-ccc8-41f1-a328-b9a5b275be6d"
const delReq = (pathToFile, callback) => {
    const options = {
        host: "192.168.1.12",
        path: pathToFile,
        port: "12023",
        method: "DELETE",
        headers: {
            "content-Type": "application/json",
            "Authorization": "Basic ZG9lZGJhOkMwREVTSE9QCg=="
        }
    }
    http.get(options, function (res) {
        let responseString = ""
    
        res.on("data", function (data) {
            responseString += data
            responseString = JSON.parse(responseString)
            console.log(responseString)
                // save all the data from response
        })
        res.on("end", () => {
            callback (responseString)
                // print to console when response ends
        })
    })
}

// getReq('/ws/policy/subsystems/', (id) => {
//     let pathToDel = '/ws/policy/subsystems/' + id
//     delReq(pathToDel, (res) => {
//         console.log(res)
//     })
// })

module.exports = delReq