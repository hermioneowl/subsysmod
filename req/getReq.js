const http = require('http')
//"ZG9lZGJhOkMwREVTSE9QCg=="


let getReq = async(pathToFile, callback) => {
    const options = {
        host: "192.168.1.12",
        path: pathToFile,
        port: "12023",
        headers: {
            "content-Type": "application/json",
            "Authorization": "Basic ZG9lZGJhOkMwREVTSE9QCg=="
        }
    }
    await http.get(options, (res) => {
        let responseString = ""
    
        res.on("data", (data) => {
            responseString += data
            
            // save all the data from response
        })
        res.on("end", () => {
            responseString = JSON.parse(responseString)
            //console.log(responseString.subsystems[0].id)
            //console.log(resInfo)
            
            //callback(responseString.subsystems[0].id)
            callback(responseString)
            // print to console when response ends
        })              
    })
}






// let subsysID = getReq('/ws/policy/subsystems/')
// console.log(subsysID)

module.exports = getReq

