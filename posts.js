const fs = require('fs')
 
const postReq = require('./req/postReq.js')
const getReq = require('./req/getReq.js')
 
 
let subsysDBCG = fs.readFileSync('./postJsons/subsysDBCG.json')
let subsysDBBG = fs.readFileSync('./postJsons/subsysDBBG.json')
let team = fs.readFileSync('./postJsons/team.json')
let application = fs.readFileSync('./postJsons/application.json')
let instance = fs.readFileSync('./postJsons/instance.json')

async function whileLoop(){
    let appStatus
    await getReq('/ws/policy/applications', (resJson) => {
        appStatus = resJson.applications[0].status
        console.log(appStatus)  
    })
    while(appStatus != 'ready'){
        await getReq('/ws/policy/applications', (resJson) => {
            appStatus = resJson.applications[0].status
            console.log(appStatus)
        })
    }
}
 
let postBody = subsysDBCG
 
let creationArray = [
   {
       'file': subsysDBCG,
       'type': 'subsystems'
   },
   {
       'file': subsysDBBG,
       'type': 'subsystems'
   },
   {
       'file': team,
       'type': 'teams'
   },
   {
       'file': application,
       'type': 'applications'
   },
   {
       'file': instance,
       'type': 'instances'
   }
]
 
 
let options = {
   host: '192.168.1.12',
   path: '/ws/policy/subsysDBCG/',
   port: '12023',
   method: 'POST',
   headers: {
       'Content-Type': 'application/json',
       "Content-Length": Buffer.byteLength(postBody),
       Accept: 'application/json',
       Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
   }
}
 
 
let subsys1Id
let subsys2Id
let teamId
let appId
 
 
postBody = subsysDBCG
 
options.path = '/ws/policy/subsystems/'
options.headers = {
   'Content-Type': 'application/json',
   'Content-Length': Buffer.byteLength(postBody),
   Accept: 'application/json',
   Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
}

postReq(postBody, options, (res) => {
   console.log(res)
   subsys1Id = res.subsystems[0].id
   console.log('Subsystem DBCG ID: ' + subsys1Id)

    //SECOND SUBSYSTEM CREATION
    postBody = subsysDBBG
 
    options.path = '/ws/policy/subsystems/'
    options.headers = {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postBody),
        Accept: 'application/json',
        Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
    }
    postReq(postBody, options, (res) => {
        //console.log(res)
        subsys2Id = res.subsystems[0].id
        console.log(subsys2Id)
    })

   //TEAM CREATION
   options.path = '/ws/policy/teams/'
    postBody = team
    postBody = JSON.parse(postBody)
    postBody.environments[0].subsystemIds[0].id = subsys1Id
    postBody = JSON.stringify(postBody)
    options.headers = {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postBody),
        Accept: 'application/json',
        Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
    }

    postReq(postBody, options, (res) => {
        console.log('TEAM HAS BEEN CREATED')
        //console.log(res)
        teamId = res.id




        options.path = '/ws/policy/applications/'
        application = JSON.parse(application)
        console.log('Team ID: ' + teamId)
        application.ownedByTeam = teamId

        
        application.db2objects.forEach((segment) => {
            segment.subsystem = subsys1Id
            segment.requiredObjects.forEach((object) => {
                object.subsystem = subsys1Id
            })
        })



        //application = JSON.parse(application)
        console.log('Subsys1Id: ' + subsys1Id)
        
        //console.log(application.db2objects[10])

        application = JSON.stringify(application)
        postBody = application
        
        options.headers = {
               'Content-Type': 'application/json',
               'Content-Length': Buffer.byteLength(postBody),
               Accept: 'application/json',
               Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
        }
        postReq(postBody, options, (res) => {
            console.log('APPLICATION CREATION STARTED')
            //console.log(res)
            appId = res.id
            //teamId = res.id
            //console.log('App ID: ' + teamId)

            
            options.path = '/ws/policy/instances/'
            application = JSON.parse(application)
            console.log('App ID: ' + appId)
            application.application = appId
            console.log ('I got to line 153')
            application = JSON.stringify(application)
            console.log ('I got to line 155')
            postBody = application
            console.log ('I got to line 157')
            options.headers = {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(postBody),
                Accept: 'application/json',
                Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
            }
            console.log ('I got to line 164')
            

            whileLoop()
            
            postReq(postBody, options, (res) => {
                console.log('INSTANCE HAS BEEN CREATED')
                console.log(res)
                flag = true
            })
            

        })
    })
});
 
 
// postBody = subsysDBBG
 
// options.path = '/ws/policy/subsystems/'
// options.headers = {
//     'Content-Type': 'application/json',
//     'Content-Length': Buffer.byteLength(postBody),
//     Accept: 'application/json',
//     Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
// }
// postReq(postBody, options, (res) => {
//     console.log(res)
//     //subsys2Id = res.subsystems[0].id
// })
 
 
 

 
 

 
 
 
 
 
 
 
 
 
 
 
//subsystems[0].id
 
 
 
// creationArray.forEach((item) => {
//     postBody = item.file
//     options.path = '/ws/policy/' + item.type + '/'
//     options.headers = {
//         'Content-Type': 'application/json',
//         'Content-Length': Buffer.byteLength(postBody),
//         Accept: 'application/json',
//         Authorization: 'Basic ZG9lZGJhOkMwREVTSE9QCg=='
//     } 
//     console.log(postBody)
//     postReq(postBody, options)
// })
 
 
// postBody = subsysDBBG
// postReq(postBody, options)
 
// postBody = team
// postReq(postBody, options)
 
// postBody = application
// postReq(postBody, options)
 
// postBody = instance
// postReq(postBody, options)
