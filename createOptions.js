//const fs = require('fs')
//let application = fs.readFileSync('./postJsons/application.json')
//'/ws/policy/applications/'
const createOptions = (path, postBody) => {
    let options = {
        host: "192.168.1.12",
        path: path, 
        port: '12023',
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Content-Length": Buffer.byteLength(postBody),
            "Accept": "application/json",
            "Authorization": "Basic ZG9lZGJhOkMwREVTSE9QCg=="
        }
    }
    return options
}
//console.log(createOptions('/ws/policy/applications/', application))
module.exports = createOptions